﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApiMethodInvocator.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   The api method invocator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.API
{
    using System;
    using System.Globalization;

    using AngularSPA.API.Route;

    /// <summary>
    /// The API method invocator.
    /// </summary>
    public static class ApiMethodInvocator
    {
        /// <summary>
        /// Invokes the appropriate API method based on path and query.
        /// </summary>
        /// <param name="path">
        /// The API method url path.
        /// </param>
        /// <param name="query">
        /// The API method url query.
        /// </param>
        /// <returns>
        /// The <see cref="string"/> with method output as JSON.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// </exception>
        public static string Invoke(string path, string query)
        {
            var routeHandler = new RouteHandler(path, query);
            Func<string> func;
            if (routeHandler.TryGetApiMethod(out func))
            {
                return func();
            }
            
            throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "Couldn't locate API method by path '{0}'", path));
        }
    }
}
