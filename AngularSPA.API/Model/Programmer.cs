﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Programmer.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   The programmer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.API.Model
{
    using System.Collections.Generic;

    /// <summary>
    /// The programmer.
    /// </summary>
    public class Programmer
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the image url.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// The programmers list mock.
        /// </summary>
        public static readonly IList<Programmer> ProgrammersListMock = new[]
                                                                  {
                                                                      new Programmer
                                                                          {
                                                                              Id = 1, 
                                                                              Name = "Dennis Ritchie", 
                                                                              Description = "Dennis MacAlistair Ritchie was an American computer scientist who “helped shape the digital era”. He created the C programming language and with long-time colleague Ken Thompson, the Unix operating system. Ritchie and Thompson received the Turing Award from the ACM in 1983, the Hamming Medal from the IEEE in 1990 and the National Medal of Technology from President Clinton in 1999. Ritchie was the head of Lucent Technologies System Software Research Department when he retired in 2007.", 
                                                                              ImageUrl = "p1.jpg"
                                                                          }, 
                                                                      new Programmer
                                                                          {
                                                                              Id = 2, 
                                                                              Name = "Bjarne Stroustrup", 
                                                                              Description = "Bjarne Stroustrup is a Danish computer scientist, most notable for the creation and development of the widely used C++ programming language. He is a Distinguished Research Professor and holds the College of Engineering Chair in Computer Science at Texas A&M University, a visiting professor at Columbia University, and works at Morgan Stanley.", 
                                                                              ImageUrl = "p2.jpg"
                                                                          }, 
                                                                      new Programmer
                                                                          {
                                                                              Id = 3, 
                                                                              Name = "James Gosling", 
                                                                              Description = "James Arthur Gosling is a Canadian computer scientist, best known as the father of the Java programming language. James has also made major contributions to several other software systems, such as NeWS and Gosling Emacs. Due to his extra-ordinary achievements Gosling was elected to Foreign Associate member of the United States National Academy of Engineering.", 
                                                                              ImageUrl = "p3.jpg"
                                                                          }, 
                                                                      new Programmer
                                                                          {
                                                                              Id = 4, 
                                                                              Name = "Linus Torvalds", 
                                                                              Description = "Linus Benedict Torvalds is a Finnish American software engineer, who was the principal force behind the development of the Linux kernel. He later became the chief architect of the Linux kernel, and now acts as the project’s coordinator. He also created the revision control system Git as well as the diving log software Subsurface. He was honored, along with Shinya Yamanaka, with the 2012 Millennium Technology Prize by the Technology Academy Finland in recognition of his creation of a new open source operating system for computers leading to the widely used Linux kernel.", 
                                                                              ImageUrl = "p4.jpeg"
                                                                          }, 
                                                                      new Programmer
                                                                          {
                                                                              Id = 5, 
                                                                              Name = "Anders Hejlsberg", 
                                                                              Description = "Anders Hejlsberg is a prominent Danish software engineer who co-designed several popular and commercially successful programming languages and development tools. He is creator of popular programming language C#. He was the original author of Turbo Pascal and the chief architect of Delphi. He currently works for Microsoft as the lead architect of C# and core developer on TypeScript.", 
                                                                              ImageUrl = "p5.jpg"
                                                                          }, 
                                                                      new Programmer
                                                                          {
                                                                              Id = 6, 
                                                                              Name = "Tim Berners-Lee", 
                                                                              Description = "Sir Timothy John “Tim” Berners-Lee also known as “TimBL,” is a British computer scientist, best known as the inventor of the World Wide Web. He made a proposal for an information management system in March 1989 and he implemented the first successful communication between a Hypertext Transfer Protocol (HTTP) client and server via the Internet. Berners-Lee is the director of the World Wide Web Consortium (W3C), which oversees the Web’s continued development.", 
                                                                              ImageUrl = "p6.jpg"
                                                                          }, 
                                                                      new Programmer
                                                                          {
                                                                              Id = 7, 
                                                                              Name = "Brian Kernighan", 
                                                                              Description = "Brian Wilson Kernighan is a Canadian computer scientist who worked at Bell Labs alongside Unix creators Ken Thompson and Dennis Ritchie and contributed to the development of Unix. He is also coauthor of the AWK and AMPL programming languages. Kernighan’s name became widely known through co-authorship of the first book on the C programming language with Dennis Ritchie.", 
                                                                              ImageUrl = "p7.jpg"
                                                                          }, 
                                                                      new Programmer
                                                                          {
                                                                              Id = 8, 
                                                                              Name = "Ken Thompson", 
                                                                              Description = "Kenneth Thompson commonly referred to as ken in hacker circles is an American pioneer of computer science. Having worked at Bell Labs for most of his career, Thompson designed and implemented the original Unix operating system. He also invented the B programming language, the direct predecessor to the C programming language, and was one of the creators and early developers of the Plan 9 operating systems. Since 2006, Thompson works at Google, where he co-invented the Go programming language.", 
                                                                              ImageUrl = "p8.jpg"
                                                                          }, 
                                                                      new Programmer
                                                                          {
                                                                              Id = 9, 
                                                                              Name = "Guido van Rossum", 
                                                                              Description = "Guido van Rossum is a Dutch computer programmer who is best known as the author of the Python programming language. In the Python community, Van Rossum is known as a “Benevolent Dictator For Life” (BDFL), meaning that he continues to oversee the Python development process, making decisions where necessary. He was employed by Google from 2005 until December 7th 2012, where he spent half his time developing the Python language. In January 2013, Van Rossum started working for Dropbox.", 
                                                                              ImageUrl = "p9.jpg"
                                                                          }, 
                                                                      new Programmer
                                                                          {
                                                                              Id = 10, 
                                                                              Name = "Donald Knuth", 
                                                                              Description = "Donald Ervin Knuth is an American computer scientist, mathematician, and Professor Emeritus at Stanford University. He is the author of the multi-volume work The Art of Computer Programming. Knuth has been called the “father” of the analysis of algorithms. He contributed to the development of the rigorous analysis of the computational complexity of algorithms and systematized formal mathematical techniques for it. In the process he also popularized the asymptotic notation. Knuth is the creator of the TeX computer typesetting system, the related METAFONT font definition language and rendering system and the Computer Modern family of typefaces.", 
                                                                              ImageUrl = "p10.jpg"
                                                                          }
                                                                  };
    }
}
