﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Company.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   The company.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.API.Model
{
    using System.Collections.Generic;

    /// <summary>
    /// The company.
    /// </summary>
    public class Company
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the revenue.
        /// </summary>
        public string Revenue { get; set; }

        /// <summary>
        /// Gets or sets the FY.
        /// </summary>
        public string FY { get; set; }

        /// <summary>
        /// Gets or sets the market cap.
        /// </summary>
        public string MarketCap { get; set; }

        /// <summary>
        /// Gets or sets the headquarters.
        /// </summary>
        public string Headquarters { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the image url.
        /// </summary>
        public string ImageUrl { get; set; }
        
        /// <summary>
        /// The companies list mock.
        /// </summary>
        public static readonly IList<Company> CompaniesListMock = new[]
                                                             {
                                                                 new Company
                                                                     {
                                                                         Id = 1, 
                                                                         Name = "Microsoft", 
                                                                         Revenue = "$86.83", 
                                                                         FY = "2015", 
                                                                         MarketCap = "$385.4", 
                                                                         Headquarters = "Redmond, WA, USA", 
                                                                         Description = "Microsoft Corporation /ˈmaɪkrɵsɒːft/[5] (commonly referred to as Microsoft) is an American multinational technology company headquartered in Redmond, Washington, that develops, manufactures, licenses, supports and sells computer software, consumer electronics and personal computers and services. Its best known software products are the Microsoft Windows line of operating systems, Microsoft Office office suite, and Internet Explorer and Edge web browsers. Its flagship hardware products are the Xbox game consoles and the Microsoft Surface tablet lineup. It is the world's largest software maker by revenue,[6] and one of the world's most valuable companies.[7] Microsoft was founded by Paul Allen and Bill Gates on April 4, 1975, to develop and sell BASIC interpreters for Altair 8800. It rose to dominate the personal computer operating system market with MS-DOS in the mid-1980s, followed by Microsoft Windows. The company's 1986 initial public offering, and subsequent rise in its share price, created three billionaires and an estimated 12,000 millionaires among Microsoft employees. Since the 1990s, it has increasingly diversified from the operating system market and has made a number of corporate acquisitions. In May 2011, Microsoft acquired Skype Technologies for $8.5 billion in its largest acquisition to date.[8] As of 2015, Microsoft is market dominant in both the IBM PC-compatible operating system (while it lost the majority of the overall operating system market to Android) and office software suite markets (the latter with Microsoft Office). The company also produces a wide range of other software for desktops and servers, and is active in areas including Internet search (with Bing), the video game industry (with the Xbox, Xbox 360 and Xbox One consoles), the digital services market (through MSN), and mobile phones (via the operating systems of Nokia's former phones[9] and Windows Phone OS). In June 2012, Microsoft entered the personal computer production market for the first time, with the launch of the Microsoft Surface, a line of tablet computers. With the acquisition of Nokia's devices and services division to form Microsoft Mobile Oy, the company re-entered the smartphone hardware market, after its previous attempt, Microsoft Kin, which resulted from their acquisition of Danger Inc.[10] Microsoft is a portmanteau of the words microcomputer and software.", 
                                                                         ImageUrl = "c1.png"
                                                                     }, 
                                                                 new Company
                                                                     {
                                                                         Id = 2, 
                                                                         Name = "Oracle", 
                                                                         Revenue = "$38.27", 
                                                                         FY = "2015", 
                                                                         MarketCap = "$194.7", 
                                                                         Headquarters = "Redwood City, CA, USA", 
                                                                         Description = "The Oracle Corporation is an American global computer technology corporation, headquartered in Redwood City, California. The company primarily specializes in developing and marketing computer hardware systems and enterprise software products – particularly its own brands of database management systems. In 2011 Oracle was the second-largest software maker by revenue, after Microsoft.[4] The company also develops and builds tools for database development and systems of middle-tier software, enterprise resource planning (ERP) software, customer relationship management (CRM) software and supply chain management (SCM) software. Larry Ellison, a co-founder of Oracle, served as Oracle's CEO from founding. On September 18, 2014, it was announced that he would be stepping down (with Mark Hurd and Safra Catz to become CEOs). Ellison became executive chairman and CTO.[5] He also served as the Chairman of the Board until his replacement by Jeffrey O. Henley in 2004. On August 22, 2008, the Associated Press ranked Ellison as the top-paid chief executive in the world.[6][7]", 
                                                                         ImageUrl = "c2.png"
                                                                     }, 
                                                                 new Company
                                                                     {
                                                                         Id = 3, 
                                                                         Name = "SAP", 
                                                                         Revenue = "$23.3", 
                                                                         FY = "2014", 
                                                                         MarketCap = "$85.9", 
                                                                         Headquarters = "Walldorf, Germany", 
                                                                         Description = "SAP SE (/ɛseɪˈpi/ or /sæp/) (Systems, Applications & Products in Data Processing) is a German multinational software corporation that makes enterprise software to manage business operations and customer relations. SAP is headquartered in Walldorf, Baden-Württemberg, Germany, with regional offices in 130 countries.[2] The company has over 293,500 customers in 190 countries.[2] The company is a component of the Euro Stoxx 50 stock market index.[3]", 
                                                                         ImageUrl = "c3.png"
                                                                     }, 
                                                                 new Company
                                                                     {
                                                                         Id = 4, 
                                                                         Name = "Symantec", 
                                                                         Revenue = "$6.7", 
                                                                         FY = "2015", 
                                                                         MarketCap = "$17.7", 
                                                                         Headquarters = "Mountain View, CA, USA", 
                                                                         Description = "Symantec Corporation /sɪˈmænˌtɛk/ (commonly known as Symantec) is an American technology company headquartered in Mountain View, California, United States. The company produces software for security, storage, backup and availability - and offers professional services to support its software. Netcraft asseses Symantec (including subsidiaries) as the most-used certification authority. Symantec is a Fortune 500 company and a member of the S&P 500 stock-market index. On October 9, 2014, Symantec declared that the company would split into two independent publicly traded companies by the end of 2015. One company would focus on security, the other on information management. The information-management business will use the name 'Veritas Technologies Corporation', echoing the name of Veritas Software (which Symantec acquired in 2005). The name Symantec represents a combination of the words 'syntax' and 'semantics'", 
                                                                         ImageUrl = "c4.jpg"
                                                                     }, 
                                                                 new Company
                                                                     {
                                                                         Id = 5, 
                                                                         Name = "VMware", 
                                                                         Revenue = "$6.0", 
                                                                         FY = "2014", 
                                                                         MarketCap = "$35.3", 
                                                                         Headquarters = "Palo Alto, CA, USA", 
                                                                         Description = "VMware, Inc. is an American company that provides cloud and virtualization software and services,[2][3][4] and claims to be the first to commercially successfully virtualize the x86 architecture.[5] Founded in 1998, VMware is based in Palo Alto, California. In 2004 it was acquired by and became a subsidiary of EMC Corporation, then on August 14, 2007, EMC sold 15% of the company in a New York Stock Exchange IPO. The company trades under the symbol VMW.[6] VMware's desktop software runs on Microsoft Windows, Linux, and Mac OS X, while its enterprise software hypervisors for servers, VMware ESX and VMware ESXi, are bare-metal hypervisors that run directly on server hardware without requiring an additional underlying operating system.[7]", 
                                                                         ImageUrl = "c5.jpg"
                                                                     }
                                                             };
    }
}
