﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Companies.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   The companies.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.API.API
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AngularSPA.API.Model;
    using AngularSPA.API.Route;

    using Newtonsoft.Json;

    /// <summary>
    /// The companies.
    /// </summary>
    [Route("companies")]
    public class Companies
    {
        /// <summary>
        /// Gets all records of current type.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [Route("get")]
        public static string Get()
        {
            return JsonConvert.SerializeObject(Company.CompaniesListMock);
        }
        
        /// <summary>
        /// Gets the record with specified ID.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [Route("byid")]
        public static string GetById(Dictionary<string, string> args)
        {
            if (!args.ContainsKey(@"id"))
            {
                throw new ArgumentException("Argument 'id' is not specified for method '/companies/byid'");
            }

            int id;
            if (!int.TryParse(args[@"id"], out id))
            {
                throw new ArgumentException("Argument 'id' for method '/companies/byid' is not an integer value");
            }

            var programmer = Company.CompaniesListMock.First(p => p.Id == id);
            return JsonConvert.SerializeObject(programmer);
        }
    }
}
