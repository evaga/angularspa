﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Programmers.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   The programmers.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.API.API
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AngularSPA.API.Model;
    using AngularSPA.API.Route;

    using Newtonsoft.Json;

    /// <summary>
    /// The programmers.
    /// </summary>
    [Route("programmers")]
    public static class Programmers
    {
        /// <summary>
        /// Gets all records of current type.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [Route("get")]
        public static string Get()
        {
            return JsonConvert.SerializeObject(Programmer.ProgrammersListMock);
        }

        /// <summary>
        /// Gets the record with specified ID.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [Route("byid")]
        public static string GetById(Dictionary<string, string> args)
        {
            if (!args.ContainsKey(@"id"))
            {
                throw new ArgumentException("Argument 'id' is not specified for method '/programmers/byid'");
            }

            int id;
            if (!int.TryParse(args[@"id"], out id))
            {
                throw new ArgumentException("Argument 'id' for method '/programmers/byid' is not an integer value");
            }

            var programmer = Programmer.ProgrammersListMock.First(p => p.Id == id);
            return JsonConvert.SerializeObject(programmer);
        }
    }
}
