﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RouteAttribute.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   Defines the RouteAttribute type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.API.Route
{
    using System;

    /// <summary>
    /// The route attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class RouteAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets the routing path.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteAttribute"/> class.
        /// </summary>
        /// <param name="path">
        /// The routing path.
        /// </param>
        public RouteAttribute(string path)
        {
            this.Path = path;
        }
    }
}
