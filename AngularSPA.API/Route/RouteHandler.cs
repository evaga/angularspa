﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RouteHandler.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   The route handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.API.Route
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// The route handler.
    /// </summary>
    public class RouteHandler
    {
        /// <summary>
        /// The API prefix.
        /// </summary>
        private const string ApiPrefix = @"/api";

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        private string Path { get; set; }

        /// <summary>
        /// Gets or sets the query.
        /// </summary>
        private string Query { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteHandler"/> class.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="query">
        /// The query.
        /// </param>
        public RouteHandler(string path, string query)
        {
            this.Path = path;
            this.Query = query;
        }

        /// <summary>
        /// Tries to get API action.
        /// </summary>
        /// <param name="func">
        /// The delegate pointing to the appropriate API method.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> value indicating whether method was found.
        /// </returns>
        public bool TryGetApiMethod(out Func<string> func)
        {
            func = null;
            if (string.IsNullOrEmpty(this.Path) || !this.Path.StartsWith(ApiPrefix))
            {
                return false;
            }

            var splits = this.Path.Split('/');
            var typeRoute = splits[2];
            if (string.IsNullOrEmpty(typeRoute))
            {
                return false;
            }

            var type = GetTypeByRoute(typeRoute);
            if (type == null)
            {
                return false;
            }

            var methodRoute = !string.IsNullOrEmpty(splits[3]) ? splits[3] : @"get";
            var method = GetMethodInfoByRoute(type, methodRoute);
            if (method == null)
            {
                return false;
            }
            
            if (!string.IsNullOrEmpty(this.Query) && this.Query.StartsWith(@"?"))
            {
                var parametrizedFunc = Delegate.CreateDelegate(typeof(Func<Dictionary<string, string>, string>), method) as Func<Dictionary<string, string>, string>;
                if (parametrizedFunc != null)
                {
                    func = () => parametrizedFunc(ArgumentsToDictionary(this.Query.Substring(1)));
                    return true;
                }

                return false;
            } 

            func = Delegate.CreateDelegate(typeof(Func<string>), method) as Func<string>;
            if (func == null)
            {
                return false;
            }
            
            return true;
        }

        /// <summary>
        /// Gets the type by route path.
        /// </summary>
        /// <param name="route">
        /// The route path.
        /// </param>
        /// <returns>
        /// The <see cref="Type"/>.
        /// </returns>
        private static Type GetTypeByRoute(string route)
        {
            var assembly = Assembly.GetExecutingAssembly();
            foreach (var type in assembly.GetTypes())
            {
                var attributes = type.GetCustomAttributes(typeof(RouteAttribute), true);
                if (attributes.OfType<RouteAttribute>().Any(routeAttribute => string.Equals(routeAttribute.Path, route, StringComparison.InvariantCultureIgnoreCase)))
                {
                    return type;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the method info by route path.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="route">
        /// The route path.
        /// </param>
        /// <returns>
        /// The <see cref="MethodInfo"/>.
        /// </returns>
        private static MethodInfo GetMethodInfoByRoute(Type type, string route)
        {
            var methods = type.GetMethods().Where(method => method.GetCustomAttributes(typeof(RouteAttribute), true).Length > 0);
            foreach (var method in methods)
            {
                var attributes = method.GetCustomAttributes(typeof(RouteAttribute), true);
                if (attributes.OfType<RouteAttribute>().Any(routeAttribute => string.Equals(routeAttribute.Path, route, StringComparison.InvariantCultureIgnoreCase)))
                {
                    return method;
                }
            }

            return null;
        }

        /// <summary>
        /// Converts the query arguments to dictionary.
        /// </summary>
        /// <param name="argumentsString">
        /// The arguments string.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary{T,T}"/>.
        /// </returns>
        private static Dictionary<string, string> ArgumentsToDictionary(string argumentsString)
        {
            var args = new Dictionary<string, string>();
            var splits = argumentsString.Split('&');
            foreach (var split in splits)
            {
                if (!string.IsNullOrEmpty(split))
                {
                    var arg = split.Split('=');
                    if (!string.IsNullOrEmpty(arg[0]) && !string.IsNullOrEmpty(arg[1]))
                    {
                        args.Add(arg[0].ToLowerInvariant(), arg[1]);
                    }
                }
            }

            return args;
        } 
    }
}
