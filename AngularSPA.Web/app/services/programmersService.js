﻿'use strict';
app.factory('programmersService', ['$http', function ($http) {
    var serviceBase = 'http://meetmeapp.cloudapp.net/';
    //var serviceBase = 'http://127.0.0.1/';
    var programmersServiceFactory = {};

    var _getAll = function () {
        return $http.get(serviceBase + 'api/programmers/get/').then(function (results) {
            return results;
        });
    };

    var _getById = function(id) {
        return $http.get(serviceBase + 'api/programmers/byid/?id=' + id).then(function (results) {
            return results;
        });
    };

    programmersServiceFactory.getAll = _getAll;
    programmersServiceFactory.getById = _getById;

    return programmersServiceFactory;
}]);