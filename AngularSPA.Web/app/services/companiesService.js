﻿'use strict';
app.factory('companiesService', ['$http', function ($http) {
    var serviceBase = 'http://meetmeapp.cloudapp.net/';
    //var serviceBase = 'http://127.0.0.1/';
    var companiesServiceFactory = {};

    var _getAll = function () {
        return $http.get(serviceBase + 'api/companies/get/').then(function (results) {
            return results;
        });
    };

    var _getById = function (id) {
        return $http.get(serviceBase + 'api/companies/byid/?id=' + id).then(function (results) {
            return results;
        });
    };

    companiesServiceFactory.getAll = _getAll;
    companiesServiceFactory.getById = _getById;

    return companiesServiceFactory;
}]);