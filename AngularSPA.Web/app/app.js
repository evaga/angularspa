﻿var app = angular.module('AngularSPA', ['ngRoute', 'angular-loading-bar', 'ui.bootstrap']);

app.config(function ($routeProvider) {

    $routeProvider.when("/home", {
        controller: "homeController",
        templateUrl: "/app/views/home.html"
    });
    
    $routeProvider.when("/programmers", {
        controller: "programmersListController",
        templateUrl: "/app/views/programmerslist.html"
    });

    $routeProvider.when("/programmer/:id", {
        controller: "programmerController",
        templateUrl: "/app/views/programmer.html"
    });

    $routeProvider.when("/companies", {
        controller: "companiesListController",
        templateUrl: "/app/views/companieslist.html"
    });

    $routeProvider.when("/company/:id", {
        controller: "companyController",
        templateUrl: "/app/views/company.html"
    });

    $routeProvider.when("/angular", {
        templateUrl: "/app/views/aboutangular.html"
    });

    $routeProvider.when("/dotnet", {
        templateUrl: "/app/views/aboutdotnet.html"
    });

    $routeProvider.otherwise({ redirectTo: "/home" });
});