﻿'use strict';
app.controller('companiesListController', ['$scope', '$location', 'companiesService', function ($scope, $location, companiesService) {
    $scope.go = function (path) {
        $location.path(path);
    };

    $scope.companies = [];
    companiesService.getAll().then(function (results) {

        $scope.companies = results.data;

    }, function (error) {
        //alert(error.data.message);
    });
}]);