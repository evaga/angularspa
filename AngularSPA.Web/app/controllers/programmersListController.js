﻿'use strict';
app.controller('programmersListController', ['$scope', '$location', 'programmersService', function ($scope, $location, programmersService) {
    $scope.go = function (path) {
        $location.path(path);
    };

    $scope.programmers = [];
    programmersService.getAll().then(function (results) {

        $scope.programmers = results.data;

    }, function (error) {
        //alert(error.data.message);
    });
}]);