﻿'use strict';
app.controller('programmerController', ['$scope', '$routeParams', 'programmersService', function ($scope, $routeParams, programmersService) {
    $scope.programmer = {};
    programmersService.getById($routeParams.id).then(function (results) {

        $scope.programmer = results.data;

    }, function (error) {
        //alert(error.data.message);
    });
}]);