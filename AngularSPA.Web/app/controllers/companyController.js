﻿'use strict';
app.controller('companyController', ['$scope', '$routeParams', 'companiesService', function ($scope, $routeParams, companiesService) {
    $scope.company = {};
    companiesService.getById($routeParams.id).then(function (results) {

        $scope.company = results.data;

    }, function (error) {
        //alert(error.data.message);
    });
}]);