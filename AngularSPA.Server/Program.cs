﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   The program.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.Server
{
    using System;

    using AngularSPA.Server.RequestListener;

    /// <summary>
    /// The program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The listening port.
        /// </summary>
        public const int Port = 80;

        /// <summary>
        /// Application entry point.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            var httpRequestListener = new HttpRequestListener(Port);
            httpRequestListener.Start();
            
            while (true)
            {
                var key = Console.ReadKey().Key;
                if ((key == ConsoleKey.Escape) && !httpRequestListener.Active)
                {
                    break;
                }

                if ((key == ConsoleKey.Escape) && httpRequestListener.Active)
                {
                    httpRequestListener.Stop();
                }

                if ((key == ConsoleKey.Enter) && !httpRequestListener.Active)
                {
                    httpRequestListener.Start();
                }
            }
        }
    }
}
