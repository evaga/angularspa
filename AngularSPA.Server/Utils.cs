﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Utils.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   The utils.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.Server
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;

    /// <summary>
    /// The utils.
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// The index files.
        /// </summary>
        private static readonly string[] Indexes = { @"index.html", @"index.htm" };

        /// <summary>
        /// Gets the working directory.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetWorkingDirectory()
        {
            return Directory.GetCurrentDirectory();
        }

        /// <summary>
        /// Tries to get file by uri.
        /// </summary>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> value indicating whether file getting was successfull.
        /// </returns>
        public static bool TryGetFileByUri(Uri uri, out FileStream file)
        {
            file = null;
            var path = string.Format(CultureInfo.InvariantCulture, "{0}{1}", GetWorkingDirectory(), uri.AbsolutePath.Replace("/", "\\"));
            if (File.Exists(path))
            {
                file = new FileStream(path, FileMode.Open);
                return true;
            }

            if (Directory.Exists(path))
            {
                foreach (var index in Indexes)
                {
                    var fileName = string.Format(CultureInfo.InvariantCulture, "{0}{1}", path, index);

                    LogToConsole(fileName);

                    if (File.Exists(fileName))
                    {
                        file = new FileStream(fileName, FileMode.Open);
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the mime type by file name.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetMimeByName(string fileName)
        {
            var mimes = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
                            {
                                {"asf", "video/x-ms-asf"}, 
                                {"asx", "video/x-ms-asf"}, 
                                {"avi", "video/x-msvideo"}, 
                                {"bin", "application/octet-stream"}, 
                                {"cco", "application/x-cocoa"}, 
                                {"crt", "application/x-x509-ca-cert"}, 
                                {"css", "text/css"}, 
                                {"deb", "application/octet-stream"}, 
                                {"der", "application/x-x509-ca-cert"}, 
                                {"dll", "application/octet-stream"}, 
                                {"dmg", "application/octet-stream"}, 
                                {"ear", "application/java-archive"}, 
                                {"eot", "application/octet-stream"}, 
                                {"exe", "application/octet-stream"}, 
                                {"flv", "video/x-flv"}, 
                                {"gif", "image/gif"}, 
                                {"hqx", "application/mac-binhex40"}, 
                                {"htc", "text/x-component"}, 
                                {"htm", "text/html"}, 
                                {"html", "text/html"}, 
                                {"ico", "image/x-icon"}, 
                                {"img", "application/octet-stream"}, 
                                {"iso", "application/octet-stream"}, 
                                {"jar", "application/java-archive"}, 
                                {"jardiff", "application/x-java-archive-diff"}, 
                                {"jng", "image/x-jng"}, 
                                {"jnlp", "application/x-java-jnlp-file"}, 
                                {"jpeg", "image/jpeg"}, 
                                {"jpg", "image/jpeg"}, 
                                {"js", "application/x-javascript"}, 
                                {"mml", "text/mathml"}, 
                                {"mng", "video/x-mng"}, 
                                {"mov", "video/quicktime"}, 
                                {"mp3", "audio/mpeg"}, 
                                {"mpeg", "video/mpeg"}, 
                                {"mpg", "video/mpeg"}, 
                                {"msi", "application/octet-stream"}, 
                                {"msm", "application/octet-stream"}, 
                                {"msp", "application/octet-stream"}, 
                                {"pdb", "application/x-pilot"}, 
                                {"pdf", "application/pdf"}, 
                                {"pem", "application/x-x509-ca-cert"}, 
                                {"pl", "application/x-perl"}, 
                                {"pm", "application/x-perl"}, 
                                {"png", "image/png"}, 
                                {"prc", "application/x-pilot"}, 
                                {"ra", "audio/x-realaudio"}, 
                                {"rar", "application/x-rar-compressed"}, 
                                {"rpm", "application/x-redhat-package-manager"}, 
                                {"rss", "text/xml"}, 
                                {"run", "application/x-makeself"}, 
                                {"sea", "application/x-sea"}, 
                                {"shtml", "text/html"}, 
                                {"sit", "application/x-stuffit"}, 
                                {"swf", "application/x-shockwave-flash"}, 
                                {"tcl", "application/x-tcl"}, 
                                {"tk", "application/x-tcl"}, 
                                {"txt", "text/plain"}, 
                                {"war", "application/java-archive"}, 
                                {"wbmp", "image/vnd.wap.wbmp"}, 
                                {"wmv", "video/x-ms-wmv"}, 
                                {"xml", "text/xml"}, 
                                {"xpi", "application/x-xpinstall"}, 
                                {"zip", "application/zip"}
                            };
            var splits = fileName.Split('.');
            var extension = splits[splits.Length - 1];

            return mimes[extension] ?? "application/octet-stream";
        }

        /// <summary>
        /// Writes the formatted message to the console.
        /// </summary>
        /// <param name="message">
        /// The message to log.
        /// </param>
        public static void LogToConsole(string message)
        {
            Console.WriteLine(string.Format(CultureInfo.CurrentCulture, "[{0}] {1}", DateTime.UtcNow, message));
        }
    }
}
