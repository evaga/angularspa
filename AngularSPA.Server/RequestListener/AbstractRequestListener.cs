﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AbstractRequestListener.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   Defines the AbstractRequestListener type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.Server.RequestListener
{
    using System.Threading;

    /// <summary>
    /// The abstract request listener.
    /// </summary>
    public abstract class AbstractRequestListener: IRequestListener
    {
        /// <summary>
        /// The active property lock.
        /// </summary>
        private object activeLock = new object();

        /// <summary>
        /// Value indicating whether listener is active.
        /// </summary>
        private bool active;

        /// <summary>
        /// Gets or sets a value indicating whether listener is active.
        /// </summary>
        public bool Active
        {
            get
            {
                lock (this.activeLock)
                {
                    return this.active;
                }
            }

            protected set
            {
                lock (this.activeLock)
                {
                    this.active = value;
                }
            }
        }

        /// <summary>
        /// Starts the listener.
        /// </summary>
        public virtual void Start()
        {
            if (!this.Active)
            {
                this.Active = true;

                new Thread(this.Loop).Start();
            }
        }

        /// <summary>
        /// Stops the listener.
        /// </summary>
        public virtual void Stop()
        {
            if (this.Active)
            {
                this.Active = false;
            }
        }

        /// <summary>
        /// The listening loop method.
        /// </summary>
        protected abstract void Loop();
    }
}
