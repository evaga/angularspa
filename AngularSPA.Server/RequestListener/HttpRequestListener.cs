﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpRequestListener.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   Defines the AbstractRequestListener type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.Server.RequestListener
{
    using System;
    using System.Globalization;
    using System.Net;

    using RequestHandler;

    /// <summary>
    /// The http request listener.
    /// </summary>
    public class HttpRequestListener : AbstractRequestListener
    {
        /// <summary>
        /// Gets or sets the listening port.
        /// </summary>
        private int Port { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpRequestListener"/> class.
        /// </summary>
        /// <param name="port">
        /// The listening port.
        /// </param>
        public HttpRequestListener(int port)
        {
            this.Port = port;
        }

        /// <summary>
        /// The listening loop method.
        /// </summary>
        protected override void Loop()
        {
            var listener = new HttpListener();
            listener.Prefixes.Add(string.Format(CultureInfo.InvariantCulture, "http://*:{0}/", this.Port));
            listener.Start();

            Utils.LogToConsole(@"Listener is started. Press 'Esc' to stop the listener.");
            while (this.Active)
            {
                try
                {
                    var requestHandler = RequestHandlerFactory.Create(listener.GetContext());
                    requestHandler.HandleRequest();
                }
                catch (Exception e)
                {
                    Utils.LogToConsole(e.Message);
                }
            }

            listener.Stop();
            Utils.LogToConsole(@"Listener is stopped. Press 'Enter' to start listener. Press 'Esc' to exit.");
        }
    }
}
