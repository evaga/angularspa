﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRequestListener.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   Defines the IRequestListener type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.Server.RequestListener
{
    /// <summary>
    /// The RequestListener interface.
    /// </summary>
    public interface IRequestListener
    {
        /// <summary>
        /// Starts the listener.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops the listener.
        /// </summary>
        void Stop();
    }
}
