﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRequestHandler.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   Defines the IRequestHandler type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.Server.RequestHandler
{
    /// <summary>
    /// The RequestHandler interface.
    /// </summary>
    public interface IRequestHandler
    {
        /// <summary>
        /// Handles the request.
        /// </summary>
        void HandleRequest();
    }
}
