﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequestHandlerFactory.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   Defines the RequestHandlerFactory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AngularSPA.Server.RequestHandler
{
    using System.IO;
    using System.Net;

    /// <summary>
    /// The request handler factory.
    /// </summary>
    public class RequestHandlerFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IRequestHandler"/> based on specified request context.
        /// </summary>
        /// <param name="context">
        /// The request context.
        /// </param>
        /// <returns>
        /// The <see cref="IRequestHandler"/>.
        /// </returns>
        public static IRequestHandler Create(HttpListenerContext context)
        {
            FileStream file;
            if (Utils.TryGetFileByUri(context.Request.Url, out file))
            {
                return new FileRequestHandler(context, file);
            }
            
            return new ApiRequestHandler(context);
        }
    }
}
