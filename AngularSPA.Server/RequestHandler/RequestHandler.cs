﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequestHandler.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   The request handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.Server.RequestHandler
{
    using System.IO;
    using System.Net;

    /// <summary>
    /// The request handler.
    /// </summary>
    public abstract class RequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets or sets the request context.
        /// </summary>
        protected HttpListenerContext Context { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestHandler"/> class.
        /// </summary>
        /// <param name="context">
        /// The request context.
        /// </param>
        protected RequestHandler(HttpListenerContext context)
        {
            this.Context = context;
        }

        /// <summary>
        /// Handles the request.
        /// </summary>
        public abstract void HandleRequest();

        /// <summary>
        /// Writes the stream to response output.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        protected void WriteStreamToOutput(Stream stream)
        {
            var buffer = new byte[1024 * 16];

            int nbytes;
            while ((nbytes = stream.Read(buffer, 0, buffer.Length)) > 0)
            {
                this.Context.Response.OutputStream.Write(buffer, 0, nbytes);
            }

            stream.Close();
            this.Context.Response.OutputStream.Flush();
        }
    }
}
