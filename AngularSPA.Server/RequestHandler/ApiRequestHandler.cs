﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApiRequestHandler.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   The api request handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.Server.RequestHandler
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;

    using AngularSPA.API;

    /// <summary>
    /// The api request handler.
    /// </summary>
    public class ApiRequestHandler : RequestHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApiRequestHandler"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public ApiRequestHandler(HttpListenerContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Handles the request.
        /// </summary>
        public override void HandleRequest()
        {
            try
            {
                var jsonResult = ApiMethodInvocator.Invoke(this.Context.Request.Url.AbsolutePath, this.Context.Request.Url.Query);
                if (!string.IsNullOrEmpty(jsonResult))
                {
                    var stream = new MemoryStream(Encoding.UTF8.GetBytes(jsonResult));
                    this.Context.Response.ContentType = @"application/json";
                    this.Context.Response.ContentLength64 = stream.Length;
                    this.Context.Response.AddHeader(@"Date", DateTime.Now.ToString("r"));

                    this.WriteStreamToOutput(stream);
                }

                this.Context.Response.StatusCode = (int)HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                Utils.LogToConsole(ex.Message);

                this.Context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            
            this.Context.Response.OutputStream.Close();
        }
    }
}
