﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileRequestHandler.cs" company="evaga">
//   AngularSPA
// </copyright>
// <summary>
//   The file request handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AngularSPA.Server.RequestHandler
{
    using System;
    using System.IO;
    using System.Net;

    /// <summary>
    /// The file request handler.
    /// </summary>
    public class FileRequestHandler : RequestHandler
    {
        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        private FileStream File { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileRequestHandler"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="file">
        /// The file.
        /// </param>
        public FileRequestHandler(HttpListenerContext context, FileStream file)
            : base(context)
        {
            this.File = file;
        }

        /// <summary>
        /// Handles the request.
        /// </summary>
        public override void HandleRequest()
        {
            try
            {
                this.Context.Response.ContentType = Utils.GetMimeByName(this.File.Name);
                this.Context.Response.ContentLength64 = this.File.Length;
                this.Context.Response.AddHeader(@"Date", DateTime.Now.ToString("r"));

                this.WriteStreamToOutput(this.File);

                this.Context.Response.StatusCode = (int)HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                Utils.LogToConsole(ex.Message);

                this.Context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            
            this.Context.Response.OutputStream.Close();
        }
    }
}
